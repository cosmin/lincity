# Lincity

This game is a revival of Lincity : http://lincity.sourceforge.net/

Python3 is used.

# Running

```shell
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
python lincity.py
```

# License

Same license as Lincity

# Thanks

Best regards go to I J Peters, Greg Sharp, Corey Keasling, original authors of this game.
