# encoding: utf-8

import pygame
import pygame.font

from game.geometry import *

BACKGROUND = (160, 48, 48)  # Palette color 224 + 6 = 230 : 12 12 12
NOJOBS = (192, 192, 0)  # Palette color 96 + 24 = 120 : 48 48 0
STARVE = (192, 0, 0)  # Palette color 32 + 24 = 56 : 48 0 0
POP = (192, 128, 0)  # Palette color 7 : 48 32 0
PPOOL = (0, 192, 192)  # Palette color 192 + 24 = 216 : 0 48 48


class Monthgraph:
    def __init__(self):
        self.position = (MONTHGRAPH_X, MONTHGRAPH_Y)
        self.surface = pygame.Surface((MONTHGRAPH_W, MONTHGRAPH_H))
        self.stats = {
            "nojobs": [None] * MONTHGRAPH_W,
            "starve": [None] * MONTHGRAPH_W,
            "pop": [None] * MONTHGRAPH_W,
            "ppool": [None] * MONTHGRAPH_W,
        }
        self.clear()
        self.refresh()

    def clear(self):
        pygame.draw.rect(self.surface, BACKGROUND, pygame.Rect(0, 0, MONTHGRAPH_W, MONTHGRAPH_H))

    def refresh(self):
        """
        Mise à jour des données des graphes (nojobs, starve, pop, ppool)
        et dessin des résultats.
        """
        for i in range(1, MONTHGRAPH_W):
            # nojobs - bargraph
            if self.stats["nojobs"][i-1] is not None:
                pygame.draw.line(
                    self.surface,
                    NOJOBS,
                    (MONTHGRAPH_W - i, MONTHGRAPH_H - self.stats["nojobs"][i] - 1),
                    (MONTHGRAPH_W - i, MONTHGRAPH_H - 1)
                )
                pygame.draw.line(
                    self.surface,
                    NOJOBS,
                    (MONTHGRAPH_W - i, MONTHGRAPH_H - self.stats["nojobs"][i-1] - 1),
                    (MONTHGRAPH_W - i, MONTHGRAPH_H - 1)
                )
                self.stats["nojobs"][i] = self.stats["nojobs"][i-1]
            # starve - bargraph
            if self.stats["starve"][i-1] is not None:
                pygame.draw.line(
                    self.surface,
                    STARVE,
                    (MONTHGRAPH_W - i, MONTHGRAPH_H - self.stats["starve"][i] - 1),
                    (MONTHGRAPH_W - i, MONTHGRAPH_H - 1)
                )
                pygame.draw.line(
                    self.surface,
                    STARVE,
                    (MONTHGRAPH_W - i, MONTHGRAPH_H - self.stats["starve"][i-1] - 1),
                    (MONTHGRAPH_W - i, MONTHGRAPH_H - 1)
                )
            # pop - plot
            if self.stats["pop"][i-1] is not None:
                pygame.draw.line(
                    self.surface,
                    POP,
                    (MONTHGRAPH_W - i, MONTHGRAPH_H - self.stats["pop"][i] - 1),
                    (MONTHGRAPH_W - i, MONTHGRAPH_H - self.stats["pop"][i] - 1)
                )
                pygame.draw.line(
                    self.surface,
                    POP,
                    (MONTHGRAPH_W - i, MONTHGRAPH_H - self.stats["pop"][i-1] - 1),
                    (MONTHGRAPH_W - i, MONTHGRAPH_H - self.stats["pop"][i-1] - 1)
                )
            # ppool - plot
            if self.stats["ppool"][i-1] is not None:
                pygame.draw.line(
                    self.surface,
                    PPOOL,
                    (MONTHGRAPH_W - i, MONTHGRAPH_H - self.stats["ppool"][i] - 1),
                    (MONTHGRAPH_W - i, MONTHGRAPH_H - self.stats["ppool"][i] - 1)
                )
                pygame.draw.line(
                    self.surface,
                    PPOOL,
                    (MONTHGRAPH_W - i, MONTHGRAPH_H - self.stats["ppool"][i-1] - 1),
                    (MONTHGRAPH_W - i, MONTHGRAPH_H - self.stats["ppool"][i-1] - 1)
                )

    def render(self, target):
        target.blit(self.surface, (self.position))
