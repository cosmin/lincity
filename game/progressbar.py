# encoding: utf-8

import pygame
import pygame.font

from game.geometry import *

BACKGROUND = (80, 160, 160)  # Palette color 224 + 20 = 244 : 40 40 40


class Progressbar:
    def __init__(self):
        self.position = (PBAR_AREA_X, PBAR_AREA_Y)
        self.surface = pygame.Surface((PBAR_AREA_W, PBAR_AREA_H))
        self.clear()
        self.refresh()

    def clear(self):
        pygame.draw.rect(self.surface, BACKGROUND, pygame.Rect(0, 0, PBAR_AREA_W, PBAR_AREA_H))

    def refresh(self):
        pass

    def render(self, target):
        target.blit(self.surface, (self.position))
