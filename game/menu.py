# encoding: utf-8

import pygame
import pygame.font

from game.geometry import *

BACKGROUND = (160, 0, 160)  # Palette color 224 + 20 = 244 : 40 40 40


class Menu:
    def __init__(self):
        self.position = (MENU_BUTTON_X, MENU_BUTTON_Y)
        self.surface = pygame.Surface((MENU_BUTTON_W, MENU_BUTTON_H * 3))
        self.clear()
        self.refresh()

    def clear(self):
        pygame.draw.rect(self.surface, BACKGROUND, pygame.Rect(0, 0, MENU_BUTTON_W, MENU_BUTTON_H * 3))

    def refresh(self):
        pass

    def render(self, target):
        target.blit(self.surface, (self.position))
