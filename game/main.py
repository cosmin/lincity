# encoding: utf-8

import pygame
import pygame.font

from game.geometry import *

BACKGROUND = (100, 100, 100)  # Palette color 224 + 20 = 244 : 40 40 40


class Main:
    def __init__(self):
        self.position = (MAIN_WIN_X, MAIN_WIN_Y)
        self.surface = pygame.Surface((MAIN_WIN_W, MAIN_WIN_H))
        self.clear()
        self.refresh()

    def clear(self):
        pygame.draw.rect(self.surface, BACKGROUND, pygame.Rect(0, 0, MAIN_WIN_W, MAIN_WIN_H))

    def refresh(self):
        pass

    def render(self, target):
        target.blit(self.surface, (self.position))
