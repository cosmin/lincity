# encoding: utf-8

import pygame
import pygame.font


class Bezel:
    def __init__(self, position=(0, 0), size=(0, 0), thickness=0, colors=None):
        self._position = position
        self._size = (size[0] + thickness * 2, size[1] + thickness * 2)
        self.thickness = thickness
        self.colors = colors or (None, None, None, None)

        self._surface = pygame.Surface((self._size[0], self._size[1]))
        if self.thickness > 0:
            for i in range(self.thickness):
                pygame.draw.line(self._surface, self.colors[0], (i, i), (self._size[0] - i, i))
                pygame.draw.line(self._surface, self.colors[1], (i, i), (i, self._size[1] - i))
                pygame.draw.line(self._surface, self.colors[2], (i, self._size[1] - i), (self._size[0] - i, self._size[1] - i))
                pygame.draw.line(self._surface, self.colors[3], (self._size[0] - i, i), (self._size[0] - i, self._size[1] - i))
        # Inner surface where user can draw
        self.surface = pygame.Surface((self._size[0] - self.thickness * 2, self._size[1] - self.thickness * 2))

    @property
    def x(self):
        return self._position[0]

    @property
    def y(self):
        return self._position[1]

    def render(self, target):
        self._surface.blit(self.surface, (self.thickness, self.thickness))
        target.blit(self._surface, (self._position))
