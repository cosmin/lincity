# encoding: utf-8

import pygame
import pygame.font

from game.geometry import *

BACKGROUND = (160, 80, 80)  # Palette color 224 + 20 = 244 : 40 40 40


class Mappoints:
    def __init__(self):
        self.position = (MAPPOINT_STATS_X, MAPPOINT_STATS_Y)
        self.surface = pygame.Surface((MAPPOINT_STATS_W, MAPPOINT_STATS_H))
        self.clear()
        self.refresh()

    def clear(self):
        pygame.draw.rect(self.surface, BACKGROUND, pygame.Rect(0, 0, MAPPOINT_STATS_W, MAPPOINT_STATS_H))

    def refresh(self):
        pass

    def render(self, target):
        target.blit(self.surface, (self.position))
