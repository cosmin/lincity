# encoding: utf-8

import pygame
import pygame.font

from game.geometry import *

BACKGROUND = (0, 80, 160)  # Palette color 224 + 20 = 244 : 40 40 40


class Status:
    def __init__(self):
        self.position = (SUST_SCREEN_X, SUST_SCREEN_Y)
        self.surface = pygame.Surface((SUST_SCREEN_W, SUST_SCREEN_H))
        self.clear()
        self.refresh()

    def clear(self):
        pygame.draw.rect(self.surface, BACKGROUND, pygame.Rect(0, 0, SUST_SCREEN_W, SUST_SCREEN_H))

    def refresh(self):
        pass

    def render(self, target):
        target.blit(self.surface, (self.position))
