# encoding: utf-8

import pygame
import pygame.font

from game.geometry import *

BACKGROUND = (0, 160, 0)  # Palette color 224 + 20 = 244 : 40 40 40


class Minimap:
    def __init__(self):
        self.position = (MINI_MAP_AREA_X, MINI_MAP_AREA_Y)
        self.surface = pygame.Surface((MINI_MAP_AREA_W, MINI_MAP_AREA_H))
        self.clear()
        self.refresh()

    def clear(self):
        pygame.draw.rect(self.surface, BACKGROUND, pygame.Rect(0, 0, MINI_MAP_AREA_W, MINI_MAP_AREA_H))

    def refresh(self):
        pass

    def render(self, target):
        target.blit(self.surface, (self.position))
