# encoding: utf-8

import pygame
import pygame.font

from game.geometry import *

BACKGROUND = (80, 160, 0)  # Palette color 224 + 20 = 244 : 40 40 40


class Select:
    def __init__(self):
        self.position = (SELECT_BUTTON_WIN_X, SELECT_BUTTON_WIN_Y)
        self.surface = pygame.Surface((SELECT_BUTTON_WIN_W, SELECT_BUTTON_WIN_H))
        self.clear()
        self.refresh()

    def clear(self):
        pygame.draw.rect(self.surface, BACKGROUND, pygame.Rect(0, 0, SELECT_BUTTON_WIN_W, SELECT_BUTTON_WIN_H))

    def refresh(self):
        pass

    def render(self, target):
        target.blit(self.surface, (self.position))
