# encoding: utf-8

import pygame
import pygame.font

from game.geometry import *

BACKGROUND = (160, 80, 0)  # Palette color 224 + 20 = 244 : 40 40 40


class Speed:
    def __init__(self):
        self.position = (SPEED_BUTTONS_X, SPEED_BUTTONS_Y)
        self.surface = pygame.Surface((SPEED_BUTTONS_W, 3 * SPEED_BUTTONS_H))
        self.clear()
        self.refresh()

    def clear(self):
        pygame.draw.rect(self.surface, BACKGROUND, pygame.Rect(0, 0, SPEED_BUTTONS_W, 3 * SPEED_BUTTONS_H))

    def refresh(self):
        pass

    def render(self, target):
        target.blit(self.surface, (self.position))
