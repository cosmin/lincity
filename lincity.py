# encoding: utf-8
"""
This game is a fork of lincity in python in order to raise it from the dead-games.

"""

from app import App
from screens import *


SCREENS = {
    "NO_SCREEN": -1,
    "SPLASH_SCREEN": 0,
    "GAME_SCREEN": 1,
}


class Game(App):
    def __init__(self):
        super().__init__("lincity", "0.1")

    def init_game(self):
        super().init_game()
        self.screens = [
            SplashScreen(self),
            GameScreen(self),
        ]


if __name__ == "__main__":
    Game().run()
