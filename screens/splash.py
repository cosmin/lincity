# encoding: utf-8

import time

import pygame
import pygame.font

from app import Screen
from lincity import SCREENS


TEXT_1 = ("Press any key to continue. A help system is available "
    "by clicking the mouse on the help button in the lower right "
    "corner of the main screen... or click the RIGHT mouse button on "
    "the things you want help with. You do not need a mouse though, "
    "the cursor keys make mouse movements, the space or return are "
    "like clicking the left mouse button, the backspace key, like the "
    "right. In dialogue boxes, return or space means OK, return, space "
    "or y means yes and backspace or n means no."
)
TEXT_2 = ("LinCity was written by I J Peters. "
    "( ijp@floot.demon.co.uk ) LinCity is copyright (C) I J Peters "
    "1995,1996. You may freely copy, distribute and modify lincity "
    "under the terms of the GNU GENERAL PUBLIC LICENSE. Please read "
    "the file COPYING for the GPL. "
    "Thanks to Mike (mike@emgee.demon.co.uk) for ideas and playing the "
    "game a lot. Thanks to Jasper (jasper@js-thorn.demon.co.uk) for "
    "the university, the 'wooden' windmills, the rockets and others."
    " Thanks to Fly for the power station and commune icons and others."
    " Also thanks to RAb for always finding those obscure bugs that "
    "only appear when you *really* try to break it."
    "Other people have contributed in some way to the development of "
    "LinCity, they may be found in the file Acknowledgements."
)
TEXT_3 = ("Lin City is a city/country simulation game. You are "
    "required to build and maintain a city. You must feed, house, "
    "provide jobs and goods for your residents. You can build a "
    "sustainable economy with the help of renewable energy and "
    "recycling, or you can go for broke and build rockets to escape "
    "from a pollution ridden and resource starved planet, it's up to "
    "you. Due to the finite resourses available in any one place, this "
    "is not a game that you can leave for long periods of time. Have "
    "fun...."
)
SCROLL_SPEED = 30
MAX_WIDTH = 500

class SplashScreen(Screen):
    def __init__(self, application):
        super().__init__(application, SCREENS["SPLASH_SCREEN"])

        self.splash = pygame.image.load("datas/splash.png").convert()
        self.timer = time.perf_counter()
        # Refreshing period in seconds
        self.speed = SCROLL_SPEED / 1000

        self.offset_1 = 0
        self.offset_2 = 0
        self.offset_3 = 0
        self.small = pygame.font.SysFont("Times New Roman", 12)
        self.large = pygame.font.SysFont("Times New Roman", 18)
        self.line_1 = self.small.render(TEXT_1, True, (255,255,255))
        self.line_2 = self.large.render(TEXT_2, True, (255,255,255))
        self.line_3 = self.large.render(TEXT_3, True, (255,255,255))

    def manage_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return SCREENS["NO_SCREEN"]
            elif event.type == pygame.KEYDOWN:
                return SCREENS["GAME_SCREEN"]
        return self.me

    def update(self):
        if self.timer + self.speed < time.perf_counter():
            self.timer = time.perf_counter()
            self.offset_1 += 1
            if self.offset_1 > self.line_1.get_width() + MAX_WIDTH:
                self.offset_1 = 0
            self.offset_2 += 2
            if self.offset_2 > self.line_2.get_width() + MAX_WIDTH:
                self.offset_2 = 0
            self.offset_3 += 2
            if self.offset_3 > self.line_3.get_width() + MAX_WIDTH:
                self.offset_3 = 0

    def render(self):
        self.app.screen.fill((0, 0, 0))
        # Render splash
        self.app.screen.blit(self.splash, (0, 0))
        # Render texts
        self.app.screen.blit(self.line_1, (150, 39), pygame.Rect(self.offset_1 - MAX_WIDTH, 0, MAX_WIDTH, 14))
        self.app.screen.blit(self.line_2, (150, 73), pygame.Rect(self.offset_2 - MAX_WIDTH, 0, MAX_WIDTH, 20))
        self.app.screen.blit(self.line_3, (150, 114), pygame.Rect(self.offset_3 - MAX_WIDTH, 0, MAX_WIDTH, 20))
