# encoding: utf-8

import time

import pygame
import pygame.font

from app import Screen
from lincity import SCREENS
import game


class GameScreen(Screen):
    def __init__(self, application):
        super().__init__(application, SCREENS["GAME_SCREEN"])

        # Refreshing period in seconds
        self.timer = time.perf_counter()
        self.delta = .25

        self.main = game.Main()
        self.mappoints = game.Mappoints()
        self.menu = game.Menu()
        self.minimap = game.Minimap()
        self.monthgraph = game.Monthgraph()
        self.progressbar = game.Progressbar()
        self.select = game.Select()
        self.speed = game.Speed()
        self.status = game.Status()

    def manage_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return SCREENS["NO_SCREEN"]
            elif event.type == pygame.KEYDOWN:
                return SCREENS["NO_SCREEN"]
        return self.me

    def update(self):
        if self.timer + self.delta < time.perf_counter():
            self.timer = time.perf_counter()
            self.monthgraph.refresh()

    def open(self):
        self.app.screen.fill((0, 0, 0))

    def render(self):
        # Rendering screen's elements
        self.main.render(self.app.screen)
        self.mappoints.render(self.app.screen)
        self.menu.render(self.app.screen)
        self.minimap.render(self.app.screen)
        self.monthgraph.render(self.app.screen)
        self.progressbar.render(self.app.screen)
        self.select.render(self.app.screen)
        self.speed.render(self.app.screen)
        self.status.render(self.app.screen)
